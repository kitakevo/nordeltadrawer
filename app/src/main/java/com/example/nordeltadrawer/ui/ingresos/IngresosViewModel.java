package com.example.nordeltadrawer.ui.ingresos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class IngresosViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public IngresosViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("MENU DE INGRESOS");
    }

    public LiveData<String> getText() {
        return mText;
    }
}